import Musicien from "./Musicien";
import Grid from '@material-ui/core/Grid';

const Musiciens =({selectedMusiciens})=>{
    return (
        <div>
             <Grid container spacing={3}>
            {selectedMusiciens.map(element => {
                return <Musicien musicien ={element} />;
            })}
            </Grid>
        </div>
    )
}

export default Musiciens;